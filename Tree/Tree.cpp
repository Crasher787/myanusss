#include <iostream>

#include <ctype.h>


template <class T> // ������
class Ysen
{
	T item;
	Ysen <T> *son;
	Ysen <T> *brother;
	//  �������� �������� � �������� �������� 

public:
	Ysen(const T & item, Ysen<T> *son = NULL, Ysen<T> *brother = NULL);

	~Ysen();

	int GetHeight();
private:

};
// ���������� ��� �������� ������ 
template<class T>
Ysen<T>::Ysen(const T & item, Ysen<T>* son, Ysen<T>* brother)
{
	Ysen<T>::item = item;
	Ysen<T>::son = son;
	Ysen<T>::brother = brother;
}

// ���������� ��� �������� ������ 
template<class T>
Ysen<T>::~Ysen()
{
	if (son)
		delete son;
	if (brother)
		delete brother;
}
//����� ���������� ������ 
template<class T>
int Ysen<T>::GetHeight()
{
	int max = 0;
	for (Ysen<T> *current = son; current; current = current->brother) {
		int curHeight = current->GetHeight();
		if (curHeight > max)
			max = curHeight;

	}

	return max + 1;
}

int main() {

	Ysen<int> *ysen = new Ysen<int>(1,
		new Ysen<int>(2, NULL,
			new Ysen<int>(100,
				new Ysen<int>(500, NULL,
					new Ysen<int>(6, new Ysen<int>(8, NULL, new Ysen<int>(9, NULL, new Ysen<int>(10, NULL, NULL))), NULL)),
				new Ysen<int>(4,
					new Ysen<int>(7, NULL, NULL), NULL))), NULL);
	//���� �������� ������ ������ �������� 1
	std::cout << "Derevo Viroslo: " << ysen->GetHeight() << std::endl;
	delete ysen;
	system("pause");
	return 0;
	
}